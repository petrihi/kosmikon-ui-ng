# kosmikon-ui-ng

Kosmikon API demo with Angular 6.

## Setup local development

Install Node.js https://nodejs.org/

Install Angular CLI https://angular.io/cli

Clone or download repository

Eg.

    $ git clone https://petrihi@bitbucket.org/petrihi/kosmikon-ui-ng.git
    $ cd kosmikon-ui-ng
    $ ng serve

Open http://localhost:4200/ in your browser.
