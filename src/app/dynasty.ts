export class Dynasty {
  id?: number;
  name?: string;
  image?: string;
  nationality?: string;
  age?: string;
  origin?: string;
  religion?: string;
  reputation?: string;
  feat?: string;
  allies?: string;
  enemies?: string;
  coatofarms?: string;
  motto?: string;
  dragonmark?: string;
  currenthead?: string;
}
