import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { Dynasty } from './dynasty';
import { DynastyList } from './dynastylist';
import { Character } from './character';
import { Family } from './family';

@Injectable({
  providedIn: 'root'
})
export class KosmikonapiService {

  constructor( private http: HttpClient ) { }

  private restUrl = environment.restUrl;

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * API call to get list of dynasties
   */
  getDynasties(): Observable<DynastyList> {
    const httpOptions = {
      params: new HttpParams().set('apicall', 'dynasty_list')
    };
    return this.http.get<DynastyList>(this.restUrl, httpOptions).pipe(
      delay(200),
      tap(_ => console.log(`fetched dynasty_list`)),
      catchError(this.handleError<DynastyList>(`dynasty_list`))
    );
  }

  /**
   * API call to get dynasty data
   */
  getDynasty(name: string): Observable<Dynasty> {
    const httpOptions = {
      params: new HttpParams().set('apicall', 'dynasty_list_attributes').set('val', `${name}`),
    };
    return this.http.get<Dynasty>(this.restUrl, httpOptions).pipe(
      delay(200),
      tap(_ => console.log(`fetched dynasty name=${name}`)),
      catchError(this.handleError<Dynasty>(`getDynasty name=${name}`))
    );
  }

  /**
   * API call to get character data
   */
  getCharacter(name: string): Observable<Character> {
    const httpOptions = {
      params: new HttpParams().set('apicall', 'character_list_attributes').set('val', `${name}`),
    };
    return this.http.get<Character>(this.restUrl, httpOptions).pipe(
      delay(200),
      tap(_ => console.log(`fetched character name=${name}`)),
      catchError(this.handleError<Character>(`getCharacter name=${name}`))
    );
  }

  /**
   * API call to get family data
   */
  getFamily(name: string): Observable<Family> {
    const httpOptions = {
      params: new HttpParams().set('apicall', 'character_list_family').set('val', `${name}`),
    };
    return this.http.get<Family>(this.restUrl, httpOptions).pipe(
      delay(200),
      tap(_ => console.log(`fetched family name=${name}`)),
      catchError(this.handleError<Family>(`getFamily name=${name}`))
    );
  }
}
