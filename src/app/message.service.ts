import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export class KosmikonUiEvent {
  type: string;
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private kuiEventSubject: Subject<KosmikonUiEvent> = new Subject<KosmikonUiEvent>();
  public readonly kosmikonUiEvent$: Observable<KosmikonUiEvent> = this.kuiEventSubject.asObservable();

  broadcast(name: string, data) {
    this.kuiEventSubject.next( {type: name,   data: data });
  }

  constructor() { }
}
