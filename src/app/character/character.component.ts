import { Component, OnInit } from '@angular/core';
import {KosmikonapiService} from '../kosmikonapi.service';
import {MessageService} from '../message.service';
import {Character} from '../character';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  character: Character;

  constructor( private apidata: KosmikonapiService, private messageService: MessageService ) {
    this.messageService.kosmikonUiEvent$.subscribe(ev => {
      if (ev.type === 'character') {
        this.getCharacter(ev.data.data);
      }
    });
  }

  ngOnInit() {
  }

  getCharacter(name: string): void {
    this.apidata.getCharacter(name)
      .subscribe(character => this.character = character);
  }
}
