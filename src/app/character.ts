export class Character {
  id?: number;
  name?: string;
  title?: string;
  image?: string;
  primarytitle?: string;
  secondarytitle?: string;
  othertitles?: string;
  reputation?: string;
  liege?: string;
  nationality?: string;
  dynasty?: string;
  religion?: string;
  organization?: string;
  birthdate?: string;
  deathdate?: string;
  alive?: boolean;
  dynasticruler?: boolean;
  dynasticheir?: boolean;
}
