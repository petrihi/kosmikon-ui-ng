import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynastylistComponent } from './dynastylist.component';

describe('DynastylistComponent', () => {
  let component: DynastylistComponent;
  let fixture: ComponentFixture<DynastylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynastylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynastylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
