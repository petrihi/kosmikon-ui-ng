import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {KosmikonapiService} from '../kosmikonapi.service';
import {Dynasty} from '../dynasty';
import {DynastyList} from '../dynastylist';
import {FormBuilder} from '@angular/forms';
import {Character} from '../character';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-dynastylist',
  templateUrl: './dynastylist.component.html',
  styleUrls: ['./dynastylist.component.css']
})
export class DynastylistComponent implements OnInit {
  dynastylist: DynastyList;
  dynasty: Dynasty;
  change = false;
  prevdynasty = '';

  dynastySelectForm = this.fb.group({
    name: ['']
  });

  constructor( private apidata: KosmikonapiService, private fb: FormBuilder, private messageService: MessageService ) { }

  ngOnInit() {
    this.getDynastyList();
  }

  wait(time: number) {
    return new Promise(resolve => setTimeout(resolve, time));
  }

  async loadMore() {
    await this.wait(300);
    if (this.dynasty) {
      if ( this.dynasty.name !== this.prevdynasty ) {
        this.messageService.broadcast('character', {data: this.dynasty.currenthead});
        this.prevdynasty = this.dynasty.name;
        this.change = false;
      }
    }
    if (this.change === true ) {
      this.loadMore();
    }
  }

  getDynastyList(): void {
    this.apidata.getDynasties()
      .subscribe(dynasties => this.dynastylist = dynasties);
  }

  getDynasty(name: string): void {
    this.apidata.getDynasty(name)
      .subscribe(dynasty => this.dynasty = dynasty);
    this.change = true;
    this.loadMore();
  }
}
