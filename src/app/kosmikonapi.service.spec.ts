import { TestBed, inject } from '@angular/core/testing';

import { KosmikonapiService } from './kosmikonapi.service';

describe('KosmikonapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KosmikonapiService]
    });
  });

  it('should be created', inject([KosmikonapiService], (service: KosmikonapiService) => {
    expect(service).toBeTruthy();
  }));
});
