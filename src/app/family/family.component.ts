import { Component, OnInit } from '@angular/core';
import { Family } from '../family';
import { KosmikonapiService } from '../kosmikonapi.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.css']
})
export class FamilyComponent implements OnInit {
  family: Family;
  character = '';

  constructor( private apidata: KosmikonapiService, private messageService: MessageService ) {
    this.messageService.kosmikonUiEvent$.subscribe(ev => {
      if (ev.type === 'character') {
        this.character = ev.data.data;
        this.getFamily(this.character);
      }
    });
  }

  ngOnInit() {
  }

  getFamily(name: string): void {
    this.apidata.getFamily(name)
      .subscribe(family => this.family = family);
  }
}
