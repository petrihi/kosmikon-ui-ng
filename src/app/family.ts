export class Family {
  id?: number;
  spouse?: string;
  children?: string;
  father?: string;
  mother?: string;
  siblings?: string;
  uncles?: string;
  cousins?: string;
}
